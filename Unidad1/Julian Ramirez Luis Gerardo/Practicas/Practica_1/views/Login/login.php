<?php
?>
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="utf-8" />
    <title>Formulario</title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
  <div class="container">
    <div class="row">
      <div class="col-sm-9 col-md-7 col-lg-5 mx-auto">
        <div class="card card-signin my-5">
          <div class="card-body">
            <h5 class="card-title text-center">Login</h5>
            <form class="form-signin">
              <div class="form-label-group">
              <label for="usr">Usuario</label>
              <p><input type="text" id="usr" class="form-control" placeholder="Nombre" required autofocus></p>
                
              </div>
              <div class="form-label-group">
              <label for="pwd">Contraseña</label>
              <p><input type="password" id="inputPassword" class="form-control" placeholder="Contraseña" required></p>
              </div>
              <button class="btn btn-lg btn-primary btn-block text-uppercase" type="submit">Ingresa</button>
              <hr class="my-4">
              </form>
          </div>
        </div>
      </div>
    </div>
  </div>
</body>
</html>