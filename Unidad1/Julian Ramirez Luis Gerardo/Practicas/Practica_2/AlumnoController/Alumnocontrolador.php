<?php
namespace App\Http\Controllers\AlumnoController;

use illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Practica2\modelo_alumno;

class Alumnocontrolador extends Controller{
    
    public function ver_alumno(){
        $practica2 = modelo_alumno::
        select('id','nombre','apellidoP','apellidoM','edad','direccion','telefono')->take(1)->first();
        return view('Alumno/ver_alumno')->with('variable',$practica2);
    }
}
?>