
<!DOCTYPE html>
<html lang="en" dir="ltr">
<head>
    <meta charset="UTF-8">
    <title></title>
    <link rel="stylesheet" href="css/app.css">
</head>
<body>
    
        <div class="container">
                <h5 class="card-title text-center ">Practica2</h5>
                <div class="row">
                            <div class="col-md-2 center-block">
                                    {!!Form::label('Nombre: ')!!}
                                    <br>
                                    {!!form::text('nombre',$variable->nombre)!!}
                            </div>                            <div class="col-md-2 ">
                                    {!!Form::label('Apellido Paterno: ')!!}
                                    <br>
                                    {!!form::text('apellidoP',$variable->apellidoP)!!}   
                            </div>
                            <div class="col-md-2 center-block">
                                    {!!Form::label('Apellido Materno: ')!!}
                                    <br>
                                    {!!form::text('apellidoM',$variable->apellidoM)!!}
                            </div>
                            <div class="col-md-2 center-block">
                                    {!!Form::label('Edad: ')!!}
                                    <br>
                                    {!!form::text('edad',$variable->edad)!!}
                            </div>                            <div class="col-md-2 ">
                                    {!!Form::label('Direccion: ')!!}
                                    <br>
                                    {!!form::text('direccion',$variable->direccion)!!}   
                            </div>
                            <div class="col-md-2 center-block">
                                    {!!Form::label('Telefono: ')!!}
                                    <br>
                                    {!!form::text('telefono',$variable->telefono)!!}
                            </div>
                 </div>
         </div> 
</body>
</html>
