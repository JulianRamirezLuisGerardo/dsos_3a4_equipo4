<?php
namespace App\Models\Practica2;
use illuminate\DataBase\Eloquent\Model;
class modelo_alumno extends model{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected $table = 'alumno';
    //Aqui va la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    //Aqui los elementos a mostrarse de la tabla en cuestion
    protected $filllable = [
        'nombre','apellidoP','apellidoM','edad','direccion','telefono'
    ];
}
?>