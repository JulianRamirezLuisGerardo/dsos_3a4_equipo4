<?php

namespace App\Models\Practica3;

use Illuminate\Database\Eloquent\Model;

class modelo_tienda extends Model
{
    protected $table = 'tienda';
    protected $primarykey = 'id';
    public $timestamps =  false;
    protected $filllable = ['id','razon_social','rfc','nombre_duenio',
    'direccion_duenio','tipo_empresa','telefono','fecha_ingreso'];
}
?>