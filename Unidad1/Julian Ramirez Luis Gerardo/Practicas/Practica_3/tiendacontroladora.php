<?php

namespace App\Http\Controllers\TiendaController;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Practica3\modelo_tienda;

class tiendacontroladora extends Controller
{
    public function ver(){
        return view('Tienda/insertar_tienda');
    }

    public function insertar(Request $request){
        $razon_social = $request->input('razon_social');
        $rfc = $request->input('rfc');
        $nombre_duenio = $request->input('nombre_duenio');
        $direccion_duenio = $request->input('direccion_duenio');
        $tipo_empresa = $request->input('tipo_empresa');
        $fecha_ingreso = $request->input('fecha_ingreso');
        $telefono = $request->input('telefono');
        modelo_tienda::insert(['razon_social'=> $razon_social,'rfc'=> $rfc,
        'nombre_duenio'=> $nombre_duenio,'direccion_duenio'=> $direccion_duenio,'tipo_empresa'=> $tipo_empresa,
        'fecha_ingreso'=> $fecha_ingreso,'telefono'=> $telefono]);
        return redirect()->to('vertienda');
    }
}
