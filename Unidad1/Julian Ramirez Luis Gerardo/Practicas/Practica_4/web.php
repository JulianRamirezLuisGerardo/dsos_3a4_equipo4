<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('actualizarp4/{id}', 'Practica4\practica4controladora@edit_datos');

Route::put('actualizar_p4/{id}','Practica4\practica4controladora@actualizar');

Route::post('insertarp4', 'Practica4\practica4controladora@insertar');

Route::get('verp4', 'Practica4\practica4controladora@ver');








Route::get('/', function () {
    return view('welcome');
});
