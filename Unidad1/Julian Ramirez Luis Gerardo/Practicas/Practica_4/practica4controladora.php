<?php
namespace App\Http\Controllers\Practica4;

use illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Practica4\modelo_practica4;

class practica4controladora extends Controller{

    public function ver(){
        return view('Practica4/insertar');
    }

    public function insertar(Request $request){
        $id = $request->input('id');
        $rfc = $request->input('rfc');
        $curp = $request->input('curp');
        $num_ctrl = $request->input('num_ctrl');
        $materia1 = $request->input('materia1');
        $materia2 = $request->input('materia2');
        $materia3 = $request->input('materia3');
        $calif_m1 = $request->input('calif_m1');
        $calif_m2 = $request->input('calif_m2');
        $calif_m3 = $request->input('calif_m3');
        $promedio = $request->input('promedio');
        $fecha = $request->input('fecha');

        modelo_practica4::insert(['id'=> $id,'rfc'=> $rfc,'curp'=> $curp,
        'num_ctrl'=> $num_ctrl,'materia1'=> $materia1,'materia2'=> $materia2,'materia3'=> $materia3,
        'calif_m1'=> $calif_m1,'calif_m2'=> $calif_m2,'calif_m3'=> $calif_m3,'promedio'=> $promedio,
        'fecha'=> $fecha]);
        return redirect()->to('verp4');
    }

    public function edit_datos($id){
        $var1 = modelo_practica4::where('id',$id)->take(1)->first();
        //$uno = modelo_alumno::find($id);
        return view('Practica4/actualizar')->with('var1',$var1); 
    }

    public function actualizar(Request $data,$id){
        $editar = modelo_practica4::find($id);
        $editar->id = $data->id;
        $editar->rfc = $data->rfc;
        $editar->curp = $data->curp;
        $editar->num_ctrl = $data->num_ctrl;
        $editar->materia1 = $data->materia1;
        $editar->materia2 = $data->materia2;
        $editar->materia3 = $data->materia3;
        $editar->calif_m1 = $data->calif_m1;
        $editar->calif_m2 = $data->calif_m2;
        $editar->calif_m3 = $data->calif_m3;
        $editar->promedio = $data->promedio;
        $editar->fecha = $data->fecha;
        $editar->save();
        return redirect()->to('actualizarp4/'.$id);
    }
}
?>