CREATE TABLE `practica4` (
 `id` int(11) NOT NULL AUTO_INCREMENT,
 `rfc` varchar(50) NOT NULL,
 `curp` varchar(50) NOT NULL,
 `num_ctrl` varchar(50) NOT NULL,
 `materia1` varchar(50) NOT NULL,
 `materia2` varchar(50) NOT NULL,
 `materia3` varchar(50) NOT NULL,
 `calif_m1` float NOT NULL,
 `calif_m2` float NOT NULL,
 `calif_m3` float NOT NULL,
 `promedio` float NOT NULL,
 `fecha` date NOT NULL,
 PRIMARY KEY (`id`)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=latin1