<?php

namespace App\Models\Practica4;

use Illuminate\Database\Eloquent\Model;

class modelo_practica4 extends Model
{
    protected $table = 'practica4';
    protected $primarykey = 'id';
    public $timestamps =  false;
    protected $filllable = ['id','rfc','curp','num_ctrl','materia1','materia2','materia3','calif_m1','calif_m2','calif_m3','promedio','fecha'];
}
?>


