<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('hola', 'PW_Controller\Micontrolador@index');

Route::get('login', 'LoginController\loginController@index');

route:: get('ver_datos','PracticaController\PracticaController@ver_datos');


Route::get('/', function () {
    return view('welcome');
});
