<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAXModels\tiendaAbarrotesEmpresa;
use App\Models\AJAXModels\tiendaAbarrotesProductos;


class ControladorTiendaAbarrotes extends Controller
{
    public function ver_formulario_abarrotes_empresa(){
        return view('AJAX/vistaInsertarEmpresa');
    }

    public function Insertar_abarrotes_empresa(Request $request){
        $id       = $request->input('id');
        $RFC  = $request->input('RFC');
        $razon_social      =$request->input('razon_social');
        $direccion_fiscal  = $request->input('domicilio_fiscal');
        $apoderado_legal  = $request->input('apoderado_legal');
        $telefono  = $request->input('telefono');

        tiendaAbarrotesEmpresa ::create (['id' => $id, 'RFC' => $RFC, 'razon_social' => $razon_social,
        'direccion_fiscal' => $direccion_fiscal, 'apoderado_legal' => $apoderado_legal, 'telefono' => $telefono]);

        return redirect()->to('abarrotes_empresa_formulario');
    
    }

    public function ver_formulario_abarrotes_productos(){
        return view('AJAX/vistaInsertarProducto');
    }

    public function Insertar_abarrotes_productos(Request $request){
        $id       = $request->input('id');
        $nombre      =$request->input('nombre');
        $tipo  = $request->input('tipo');
        $proveedor  = $request->input('proveedor');
        $precio_unitario  = $request->input('precio_unitario');
        $precio_venta  = $request->input('precio_venta');

        tiendaAbarrotesProductos ::create (['id' => $id, 'nombre' => $nombre,
         'tipo' => $tipo,'proveedor' => $proveedor, 'precio_unitario' => $precio_unitario,
        'precio_venta' => $precio_venta]);

        return redirect()->to('abarrotes_producto_formulario');
    
    }

    public function listar_precio($pre)
    {
        $al = tiendaAbarrotesProductos::select('id','precio_venta')
        ->where('id',$pre)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = tiendaAbarrotesProductos::pluck('nombre','id');
        return view('AJAX/vistaVerProductos')->with('sem',$enviar);
    }


}
?>