<?php

namespace App\Models\AJAXModels;

use Illuminate\Database\Eloquent\Model;

class tiendaAbarrotesEmpresa extends Model
{
    //aqui se declara el nombre de la tabla que esta en mysql
    protected  $table = 'tienda_abarrotes_empresa';
    // aqui la llave primaria de la tabla
    protected $primarykey = 'id';
    public $timestamps = false;
    // aqui los elementos a mostrarse de la tabla en cuestion
    protected $fillable = [
      'id', 'RFC', 'razon_social', 'direccion_fiscal', 'apoderado_legal', 'telefono'
    ];
}
