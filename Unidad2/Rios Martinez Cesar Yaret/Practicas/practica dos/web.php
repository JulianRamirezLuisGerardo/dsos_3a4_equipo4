<?php

Route::get('/', function () {
    return view('welcome');
});

//Route::get('ver_datos','PracticaController\PracticaController@ver_datos');

Route::get('visualizar','PracticaController\PracticaController@ver_datos');

Route::get('formulario','PracticaController\PracticaController@ver_formulario');

Route::post('insertar','PracticaController\PracticaController@insertar');

Route::get('actualizar/{id}','PracticaController\PracticaController@edit_datos');

Route::put('actualizar_datos/{id}','PracticaController\PracticaController@actualizar_datos');

Route::get('eliminar/{id}','PracticaController\PracticaController@edit_datos_eliminar');

Route::put('eliminar_datos/{id}','PracticaController\PracticaController@eliminar_datos');

Route::get('eliminar_bandera/{id}','PracticaController\PracticaController@editar_bandera');

Route::put('bandera_eliminar/{id}','PracticaController\PracticaController@bander_eliminado');

Route::get('tabla','PracticaController\PracticaController@ver_tabla');

//AJAX

Route::get('lista_alumnos/{genero}','AJAx\AjaxController@listado_alumnos');
Route::get('ajax','AJAx\AjaxController@formu');
Route::get('ajaxSemestre','AJAx\AjaxController@formuSemestre');
Route::get('lista_materias/{materias}','AJAx\AjaxController@listado_semestre');
Route::post('insertar','AJAx\AjaxController@insertar');
//TIENDADEABARROTES
Route::post('insertartienda', 'AJAx\AjaxController@insertar');
Route::get('vertienda', 'AJAx\AjaxController@ver_tienda');

Route::post('insertarproducto', 'AJAx\AjaxController@insertarproducto');
Route::get('verproducto', 'AJAx\AjaxController@ver_producto');

Route::get('lista_produc/{genero}','AJAx\AjaxController@listado_productos');
Route::get('ajaxproductos','AJAx\AjaxController@formuproducto');





Route::get('/', function () {
    return view('welcome');
});

