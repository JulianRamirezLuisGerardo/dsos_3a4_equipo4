<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\Alumno;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre;
use  App\Models\AJAX\Tiendadeabarrotes;
use App\Models\AJAX\Producto;


class AjaxController extends Controller
{
    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function listado_semestre($materias)
    {
        $al = Materias::select('id','materia','semestre')
        ->where('semestre',$materias)
        ->get();
          return $al;
    }

    public function formuSemestre(){
        $enviar = Semestre::pluck('semestre','id');
        return view('AJAX/ejemplo2')->with('semestre',$enviar);
    }

    public function insertar(Request $request)
    {
        //$id = 0;
        $rfc = $request->input('rfc');
        $razonSocial = $request->input('razonSocial');
        $direccionfiscal = $request->input('direccionfiscal');
        $apoderadolegal = $request->input('apoderadolegal');
        $telefono = $request->input('telefono'); 

        Tiendadeabarrotes::create([/*'id' => 0,*/'rfc' => $rfc,'razonSocial' => $razonSocial,
      'direccionfiscal' => $direccionfiscal,'apoderadolegal' => $apoderadolegal,'telefono' => $telefono]); 
      return redirect()->to('vertienda');
    }

    public function ver_tienda(){
        return view('AJAX/tienda');
    }

    public function insertarproducto(Request $request)
    {
        //$id = 0;
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $proveedor = $request->input('proveedor');
        $preciou = $request->input('preciou');
        $preciov = $request->input('preciov'); 

        Producto::create(['nombre' => $nombre,'tipo' => $tipo,
      'proveedor' => $proveedor,'preciou' => $preciou,'preciov' => $preciov]); 
      return redirect()->to('verproducto');
    }

    public function ver_producto(){
        return view('AJAX/producto');
    }


    public function listado_productos($genero)
    {
        $al = Producto::select('id','preciov')
        ->where('id',$genero)
        ->get();
          return $al;
    }

    public function formuproducto()
    {
        $enviar = Producto::pluck('nombre','id');
        return view('AJAX/visualizarproducto')->with('sex',$enviar);
    }
}
?>
