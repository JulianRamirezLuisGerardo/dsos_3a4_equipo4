<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\Alumno;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre; 


class AjaxController extends Controller
{
    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function listado_semestre($materias)
    {
        $al = Materias::select('id','materia','semestre')
        ->where('semestre',$materias)
        ->get();
          return $al;
    }

    public function formuSemestre(){
        $enviar = semestre::pluck('semestre','id');
        return view('AJAX/ejemplo2')->with('semestre',$enviar);
    }

    public function insertar(Request $request)
    {
        //$id = 0;
        $nombre_alumno = $request->input('nombre_alumno');
        $numero_control = $request->input('numero_control');
        $semestre = $request->input('idsemestre');
        $materia = $request->input('idmateria'); 

      alumno::create([/*'id' => 0,*/'nombre_alumno' => $nombre_alumno,'numero_control' => $numero_control,
      'semestre' => $semestre,'materia' => $materia]); 
      return redirect()->to('ajaxSemestre');
    }
}
?>