<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//AJAX MATERIAS
Route::get('ajaxSemestre','AJAx\AjaxController@formuSemestre');
Route::get('lista_materias/{materias}','AJAx\AjaxController@listado_semestre');
Route::post('insertar','AJAx\AjaxController@insertar');

Route::get('/', function () {
    return view('welcome');
});
