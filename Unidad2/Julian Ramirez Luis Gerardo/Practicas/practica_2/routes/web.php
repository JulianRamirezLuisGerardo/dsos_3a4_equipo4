<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TIENDADEABARROTES
Route::post('insertartienda', 'AJAx\AjaxController@insertar');
Route::get('vertienda', 'AJAx\AjaxController@ver_tienda');

Route::post('insertarproducto', 'AJAx\AjaxController@insertarproducto');
Route::get('verproducto', 'AJAx\AjaxController@ver_producto');

Route::get('lista_produc/{genero}','AJAx\AjaxController@listado_productos');
Route::get('ajaxproductos','AJAx\AjaxController@formuproducto');





Route::get('/', function () {
    return view('welcome');
});
