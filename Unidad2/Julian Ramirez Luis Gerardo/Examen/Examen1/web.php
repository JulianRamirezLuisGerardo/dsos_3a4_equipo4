<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//TIENDADEABARROTES
Route::post('insertartienda', 'AJAx\AjaxController@insertar');
Route::get('vertienda', 'AJAx\AjaxController@ver_tienda');

Route::post('insertarproducto', 'AJAx\AjaxController@insertarproducto');
Route::get('verproducto', 'AJAx\AjaxController@ver_producto');

Route::get('lista_produc/{genero}','AJAx\AjaxController@listado_productos');
Route::get('ajaxproductos','AJAx\AjaxController@formuproducto');

//examen sorpresa
//editar tienda
Route::get('actualizartienda/{id}', 'AJAx\AjaxController@edit_datostienda');
Route::put('actualizar_tienda/{id}','AJAx\AjaxController@actualizartienda');

//eliminar tienda
Route::get('eliminar_bandera/{id}','AJAx\AjaxController@editar_bandera');
Route::put('eliminar_datostienda/{id}', 'AJAx\AjaxController@bandera_eliminar');

Route::get('/', function () {
    return view('welcome');
});
