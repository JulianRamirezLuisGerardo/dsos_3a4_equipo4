<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\Alumno;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre;
use  App\Models\AJAX\Tiendadeabarrotes;
use App\Models\AJAX\Producto;


class AjaxController extends Controller
{
    public function insertar(Request $request)
    {
        //$id = 0;
        $rfc = $request->input('rfc');
        $razonSocial = $request->input('razonSocial');
        $direccionfiscal = $request->input('direccionfiscal');
        $apoderadolegal = $request->input('apoderadolegal');
        $telefono = $request->input('telefono'); 

        Tiendadeabarrotes::create([/*'id' => 0,*/'rfc' => $rfc,'razonSocial' => $razonSocial,
      'direccionfiscal' => $direccionfiscal,'apoderadolegal' => $apoderadolegal,'telefono' => $telefono]); 
      return redirect()->to('vertienda');
    }

    public function ver_tienda(){
        return view('AJAX/tienda');
    }

    public function insertarproducto(Request $request)
    {
        //$id = 0;
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $proveedor = $request->input('proveedor');
        $preciou = $request->input('preciou');
        $preciov = $request->input('preciov'); 

        Producto::create(['nombre' => $nombre,'tipo' => $tipo,
      'proveedor' => $proveedor,'preciou' => $preciou,'preciov' => $preciov]); 
      return redirect()->to('verproducto');
    }

    public function ver_producto(){
        return view('AJAX/producto');
    }


    public function listado_productos($genero)
    {
        $al = Producto::select('id','nombre','tipo','proveedor','preciou','preciov')
        ->where('id',$genero)
        ->get();
          return $al;
    }

    public function formuproducto()
    {
        $enviar = Producto::pluck('nombre','id');
        return view('AJAX/visualizarproducto')->with('sex',$enviar);
    }


    //examen sorpresa
    //editarempresa    
    public function edit_datostienda($id){
        $var1 = Tiendadeabarrotes::where('id',$id)->take(1)->first();
        return view('AJAX/editarempresa')->with('uno',$var1); 
    }

    public function actualizartienda(Request $data,$id){
        $editar = Tiendadeabarrotes::find($id);
        $editar->id = $data->id;
        $editar->rfc = $data->rfc;
        $editar->razonSocial = $data->razonSocial;
        $editar->direccionfiscal = $data->direccionfiscal;
        $editar->apoderadolegal = $data->apoderadolegal;
        $editar->telefono = $data->telefono;
        $editar->save();
        return redirect()->to('actualizartienda/'.$id);
    }

    //eliminar empresa
    public function editar_bandera($id){
        $var1 = Tiendadeabarrotes::
        where('id',$id)->take(1)->first();
        return view('AJAX/eliminartienda')
            ->with('var1',$var1); 
    }

    public function bandera_eliminar($id){
        $editar = Tiendadeabarrotes::find($id);
        $editar->bandera = '0';
        $editar->save();
        return redirect()->to('vertienda');
    }    
}
?>