$(function() {
    $('#select-genero').on('change', metodo_listar);

    $('#idcantidadtxt').on('change', metodo_calcular);
});
var total;

function metodo_listar() {
    var genero = document.getElementById("select-genero").value;
    $.get('lista_produc/' + genero + '', function(data) {
        var html_select = null; //'<option value="">SELECCIONE</option>';
        for (var i = 0; i < data.length; i++) {
            html_select += '<option value="' + data[i].id + '">' + data[i].preciov + '</option>'
            total = data[i].preciov;
        }
        $('#select-genero2').html(html_select);
        document.getElementById("idtotaltxt").value = '';
        document.getElementById("idcantidadtxt").value = '';

    });
}

function metodo_calcular() {
    var cantidad = document.getElementById("idcantidadtxt").value;
    var totalner = cantidad * total;
    var inputNombre = document.getElementById("idtotaltxt");
    inputNombre.value = totalner;
}