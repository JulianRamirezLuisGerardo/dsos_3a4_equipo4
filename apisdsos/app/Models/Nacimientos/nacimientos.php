<?php

namespace App\Models\Nacimientos;

use Illuminate\Database\Eloquent\Model;

class Nacimientos extends Model
{
    protected $table="nacimientos";
    protected $primaryKey="id";
    public $timestamps=false;
    protected $fillable=["edo_captura","fecha_nac_madre","edad_madre","estado_conyugal","entidad_residencia_madre",
    "numero_embarazos","hijos_nacidos_muertos","hijos_nacidos_vivos","hijos_sobrevivientes",
    "fecha_nacimiento_nac_vivo","hora_nacimiento_nac_vivo","sexo_nac_vivo",
    "talla_nac_vivo","peso_nac_vivo","lugar_de_nacimiento" ,"nombre_unidad_medica",
    "entidad_nacimiento","certificado_por","entidad_certifico","fecha_certificacion"];
}
 