<?php

namespace App\Http\Controllers\Auth;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Nacimientos\nacimientos;
use Illuminate\Foundation\Auth\AuthenticatesUsers;

use GuzzleHttp\Client;
use GuzzleHttp\Exception\GuzzleException;


class LoginController extends Controller
{
    /*
    |--------------------------------------------------------------------------
    | Login Controller
    |--------------------------------------------------------------------------
    |
    | This controller handles authenticating users for the application and
    | redirecting them to your home screen. The controller uses a trait
    | to conveniently provide its functionality to your applications.
    |
    */

    use AuthenticatesUsers;

    /**
     * Where to redirect users after login.
     *
     * @var string
     */
    protected $redirectTo = '/home';

    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('guest')->except('logout');
    }

    public function busqueda(Request $request){
        $nombre=$request->id;
        return response()->json($nombre=nacimientos::select("id","edo_captura","fecha_nac_madre",
        "edad_madre","estado_conyugal","entidad_residencia_madre",
        "numero_embarazos","hijos_nacidos_muertos","hijos_nacidos_vivos","hijos_sobrevivientes",
        "fecha_nacimiento_nac_vivo","hora_nacimiento_nac_vivo","sexo_nac_vivo",
        "talla_nac_vivo","peso_nac_vivo","lugar_de_nacimiento" ,"nombre_unidad_medica",
        "entidad_nacimiento","certificado_por","entidad_certifico","fecha_certificacion")
        ->where('id','=',$nombre)
        ->take(1)->first());
    }

    public function insertar(Request $request){
       $edo_captura=$request->edo_captura;
       $fecha_nac_madre=$request->fecha_nac_madre;
       $edad_madre=$request->edad_madre;
       $estado_conyugal=$request->estado_conyugal;
       $entidad_residencia_madre=$request->entidad_residencia_madre;
       $numero_embarazos=$request->numero_embarazos;
       $hijos_nacidos_muertos=$request->hijos_nacidos_muertos;
       $hijos_nacidos_vivos=$request->hijos_nacidos_vivos;
       $hijos_sobrevivientes=$request->hijos_sobrevivientes;
       $fecha_nacimiento_nac_vivo=$request->fecha_nacimiento_nac_vivo;
       $hora_nacimiento_nac_vivo=$request->hora_nacimiento_nac_vivo;
       $sexo_nac_vivo=$request->sexo_nac_vivo;
       $talla_nac_vivo=$request->talla_nac_vivo;
       $peso_nac_vivo=$request->peso_nac_vivo;
       $lugar_de_nacimiento=$request->lugar_de_nacimiento;
       $nombre_unidad_medica=$request->nombre_unidad_medica;
       $entidad_nacimiento=$request->entidad_nacimiento;
       $certificado_por=$request->certificado_por;
       $entidad_certifico=$request->entidad_certifico;
       $fecha_certificacion=$request->fecha_certificacion;
       nacimientos::create([
           'edo_captura'=>$edo_captura,'fecha_nac_madre'=>$fecha_nac_madre,'edad_madre'=>$edad_madre,
           'estado_conyugal'=>$estado_conyugal,'entidad_residencia_madre'=>$entidad_residencia_madre,
           'numero_embarazos'=>$numero_embarazos,'hijos_nacidos_muertos'=>$hijos_nacidos_muertos,
           'hijos_nacidos_vivos'=>$hijos_nacidos_vivos,'hijos_sobrevivientes'=>$hijos_sobrevivientes,
           'fecha_nacimiento_nac_vivo'=>$fecha_nacimiento_nac_vivo,'hora_nacimiento_nac_vivo'=>$hora_nacimiento_nac_vivo,
           'sexo_nac_vivo'=>$sexo_nac_vivo,'talla_nac_vivo'=>$talla_nac_vivo,'peso_nac_vivo'=>$peso_nac_vivo,
           'lugar_de_nacimiento'=>$lugar_de_nacimiento,'nombre_unidad_medica'=>$nombre_unidad_medica,'entidad_nacimiento'=>$entidad_nacimiento,
           'certificado_por'=>$certificado_por,'entidad_certifico'=>$entidad_certifico,'fecha_certificacion'=>$fecha_certificacion
       ]);
       return response()->json(['Mensaje'=>'Registrado correctamente',]);
    }
    public function editar(Request $request){
        $id=$request->id;
         $edo_captura=$request->edo_captura;
         $editar=nacimientos::select('id',"edo_captura")
         ->where('id','=',$id)
         ->update(['edo_captura'=>$edo_captura]);
         return response()->json(['Mensaje'=>'Actualizado correctamente',]);
      }

      public function eliminar(Request $request){
        $id=$request->id;
        $editar=nacimientos::select('id',"edo_captura","fecha_nac_madre","edad_madre","estado_conyugal","entidad_residencia_madre",
        "numero_embarazos","hijos_nacidos_muertos","hijos_nacidos_vivos","hijos_sobrevivientes",
        "fecha_nacimiento_nac_vivo","hora_nacimiento_nac_vivo","sexo_nac_vivo",
        "talla_nac_vivo","peso_nac_vivo","lugar_de_nacimiento" ,"nombre_unidad_medica",
        "entidad_nacimiento","certificado_por","entidad_certifico","fecha_certificacion")
        ->where('id','=',$id)
        ->delete(); 
        return response()->json(['Mensaje'=>'Eliminado correctamente',]);
     }

     public function obtieneNac($id){
        $respuesta = $this->peticion('GET',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/ver_nacimientos/{$id}");
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }
    // public function apiRegistraNac(){
    //     $respuesta=$this->peticion ('post',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/insertar_nacimiento",[
    //         'headers' =>[
    //             'Content-Type'=>'application/x-www-form-urlencoded',
    //             'X-Requested-Whit'=>'XMLHttpRequest'
    //         ],
    //             'form-params'=>[
    //                 'edo_captura'=> 'Oaxaca',
    //                 'fecha_nac_madre'=> '1992-07-21',
    //                 'edad_madre'=> '24',
    //                 'estado_conyugal'=> 'UNIÓN LIBRE',
    //                 'entidad_residencia_madre'=> 'AGUASCALIENTES',
    //                 'numero_embarazos'=> '1',
    //                 'hijos_nacidos_muertos'=> '0',
    //                 'hijos_nacidos_vivos'=> '1',
    //                 'hijos_sobrevivientes'=> '1',
    //                 'fecha_nacimiento_nac_vivo'=> '2017-01-30',
    //                 'hora_nacimiento_nac_vivo'=> '21:30',
    //                 'sexo_nac_vivo'=> 'MUJER',
    //                 'talla_nac_vivo'=> '49',
    //                 'peso_nac_vivo'=> '2650',
    //                 'lugar_de_nacimiento'=> 'IMSS',
    //                 'nombre_unidad_medica'=> 'HGZ 47 VICENTE GUERRERO',
    //                 'entidad_nacimiento'=> 'DISTRITO FEDERAL',
    //                 'certificado_por'=> 'MÉDICO PEDIATRA',
    //                 'entidad_certifico'=> 'DISTRITO FEDERAL',
    //                 'fecha_certificacion'=> '2017-01-30'
    //             ]
    //     ]);
    //     $datos = json_decode($respuesta);
    //        return response()->json($datos);
    // }
    
   // public function apiActualizarNac($id){
    //     $respuesta = $this->peticion('put',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/actualizar_nacimiento/{$id}",[
    //         'header'=>[
    //             'Content-Type'=>'application/x-www-form-urlencoded',
    //             'X-Requested-Whit'=>'XMLHttpRequest'
    //         ],
    //         'form-params'=>[
    //             'id'=> $id,
    //             'edo_captura'=> 'Oaxaca'
    //         ]
    //     ]);     
    //     $datos = json_decode($respuesta);
    //     return response()->json($datos);
    //    }

       public function apiEliminarNac($id){
        $respuesta = $this->peticion('put',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/eliminar_nacimiento/{$id}");
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }

    public function apiRegistraNac(){
        $respuesta = $this->peticion('POST',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/insertar_nacimiento",[
            'headers'=>[
                'content-Type'=>'application/x-www-form-urlencoded',
                'X-Requested-With'=>'XMLHTTPRequest'],
            'form_params'=>[
                'edo_captura'=> 'PruebaEnClase',
                    'fecha_nac_madre'=> '1992-07-21',
                    'edad_madre'=> '24',
                    'estado_conyugal'=> 'UNIÓN LIBRE',
                    'entidad_residencia_madre'=> 'AGUASCALIENTES',
                    'numero_embarazos'=> '1',
                    'hijos_nacidos_muertos'=> '0',
                    'hijos_nacidos_vivos'=> '1',
                    'hijos_sobrevivientes'=> '1',
                    'fecha_nacimiento_nac_vivo'=> '2017-01-30',
                    'hora_nacimiento_nac_vivo'=> '21:30',
                    'sexo_nac_vivo'=> 'MUJER',
                    'talla_nac_vivo'=> '49',
                    'peso_nac_vivo'=> '2650',
                    'lugar_de_nacimiento'=> 'IMSS',
                    'nombre_unidad_medica'=> 'HGZ 47 VICENTE GUERRERO',
                    'entidad_nacimiento'=> 'DISTRITO FEDERAL',
                    'certificado_por'=> 'MÉDICO PEDIATRA',
                    'entidad_certifico'=> 'DISTRITO FEDERAL',
                    'fecha_certificacion'=> '2017-01-30'
 
            ]
        ]);
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }

    public function apiActualizarNac($id){
        $respuesta = $this->peticion('PUT',"https://tranquil-wildwood-71320.herokuapp.com/api/auth/actualizar_nacimiento/{$id}",[
            'headers'=>[
                'content-Type'=>'application/x-www-form-urlencoded',
                'X-Requested-With'=>'XMLHTTPRequest'],
            'form_params'=>[
                'id'=> $id,
                'edo_captura'=> 'Oaxaca'
 
            ]
        ]);
        $datos = json_decode($respuesta);
        return response()->json($datos);
    }

}
