<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('verdato/{id}','Auth\LoginController@obtieneNac');

Route::get('verdato2/{id}','Auth\LoginController@obtieneApi2');

Route::get('insertar','Auth\LoginController@apiRegistraNac');

Route::get('consumir','Auth\LoginController@consumir');


Route::get('actualizar/{id}','Auth\LoginController@apiActualizarNac');

Route::get('eliminar/{id}','Auth\LoginController@apiEliminarNac');

Route::get('/', function () {
    return view('welcome');
});
