<?php

use Illuminate\Http\Request;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::group(['prefix'=>'auth'],function(){
    Route::get('ver_nacimientos/{id}','Auth\LoginController@busqueda');
    Route::post('insertar_nacimiento','Auth\LoginController@insertar');
    Route::put('actualizar_nacimiento/{id}','Auth\LoginController@editar');
    Route::put('eliminar_nacimiento/{id}','Auth\LoginController@eliminar');
    
});