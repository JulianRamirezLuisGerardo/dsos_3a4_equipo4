<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('hola', 'PW_Controller\Micontrolador@index'); 

Route::get('login', 'LoginController\loginController@index');
Route::get('ver_datos', 'PW_Controller\Micontrolador@ver_datos');

Route::get('ver_datos2', 'PW_Controller\Micontrolador@ver_datos2');
Route::get('ver_alumno', 'AlumnoController\Alumnocontrolador@ver_alumno');

Route::post('insertar', 'AlumnoController\Alumnocontrolador@insertar');
Route::get('visualizar', 'AlumnoController\Alumnocontrolador@ver_formulario');

Route::post('insert_store', 'TiendaController\Tiendacontroladora@insertar');
Route::get('vertienda', 'TiendaController\Tiendacontroladora@ver');

Route::get('actualizar/{id}', 'AlumnoController\Alumnocontrolador@edit_datos');
Route::put('actualizar_datos/{id}','AlumnoController\Alumnocontrolador@actualizar');

Route::get('actualizarp4/{id}', 'Practica4\practica4controladora@edit_datos');
Route::put('actualizar_p4/{id}','Practica4\practica4controladora@actualizar');

Route::post('insertarp4', 'Practica4\practica4controladora@insertar');
Route::get('verp4', 'Practica4\practica4controladora@ver');

Route::get('eliminar_p4/{id}','Practica4\practica4controladora@edit_datos_eliminar');
Route::put('eliminar_datos/{id}', 'Practica4\practica4controladora@eliminar_datos');

Route::get('eliminar_bandera/{id}','Practica4\practica4controladora@editar_bandera');
Route::put('bandera_eliminar/{id}', 'Practica4\practica4controladora@bandera_eliminar');

Route::get('vertabla','Practica4\practica4controladora@ver_tabla');

//AJAX

Route::get('lista_alumnos/{genero}','AJAx\AjaxController@listado_alumnos');
Route::get('ajax','AJAx\AjaxController@formu');

//AJAX MATERIAS
//ruta del formulario para insertar controlador AjaxController
Route::get('ajaxSemestre','AJAx\AjaxController@formuSemestre');
//ruta 
Route::get('lista_materias/{materias}','AJAx\AjaxController@listado_semestre');
//ruta
Route::post('insertar','AJAx\AjaxController@inserta');
//----------------------------------------------------------------------------------------
//SEGUNDAS
Route::get('ajaxProductos1','AJAx\AjaxController@formuProductos');

//ruta insertar
Route::post('insertar1','AJAx\AjaxController@inserta1');

//eliminar por completo
Route::get('eliminar_pros/{id}','AJAx\AjaxController@eliminar_producto');
Route::put('eliminar_total_pro/{id}','AJAx\AjaxController@eliminate');

//actualizar
Route::get('actualizarP/{id}', 'AJAx\AjaxController@edit_datosP');
Route::put('actualizar_P/{id}','AJAx\AjaxController@actualizarP');

//eliminar por bandera
Route::get('eliminar_banderaP/{id}', 'AJAx\AjaxController@eliminar_datosP');
Route::put('eliminar_b/{id}','AJAx\AjaxController@eliminar_P');
//-----------------------------------------------------------------------------------------
//----------------------------------------------------------------------------------------


//TIENDADEABARROTES
Route::post('insertartienda', 'AJAx\AjaxController@insertar');
Route::get('vertienda', 'AJAx\AjaxController@ver_tienda');

Route::post('insertarproducto', 'AJAx\AjaxController@insertarproducto');
Route::get('verproducto', 'AJAx\AjaxController@ver_producto');

Route::get('lista_produc/{genero}','AJAx\AjaxController@listado_productos');
Route::get('ajaxproductos','AJAx\AjaxController@formuproducto');

//examen sorpresa
//editar tienda
Route::get('actualizartienda/{id}', 'AJAx\AjaxController@edit_datostienda');
Route::put('actualizar_tienda/{id}','AJAx\AjaxController@actualizartienda');

//eliminar tienda
Route::get('eliminar_bandera/{id}','AJAx\AjaxController@editar_bandera');
Route::put('eliminar_datostienda/{id}', 'AJAx\AjaxController@bandera_eliminar');

Route::get('/', function () {
    return view('PROYECTO\principal1');
});

Route::get('/prod', function () {
    return view('PROYECTO\productos');
});

//segundas
Route::get('ajaxformulario','AJAx\AjaxController@formularioa');
