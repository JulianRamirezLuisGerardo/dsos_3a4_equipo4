<?php

namespace App\Http\Controllers\AJAx;

use Illuminate\Http\Request;

use App\Http\Controllers\Controller;

use App\Models\AJAX\Sexo;
use App\Models\AJAX\Alumnos;
use App\Models\AJAX\Alumno;
use App\Models\AJAX\Materias;
use App\Models\AJAX\Semestre;
use  App\Models\AJAX\Tiendadeabarrotes;
use App\Models\AJAX\Producto;
use App\Models\AJAX\Producto1;


class AjaxController extends Controller
{
    public function listado_alumnos($genero)
    {
        $al = Alumnos::select('id','nombre_completo','sexo')
        ->where('sexo',$genero)
        ->get();
          return $al;
    }

    public function formu()
    {
        $enviar = Sexo::pluck('sexo','id');
        return view('AJAX/ejemplo1')->with('sex',$enviar);
    }

    public function listado_semestre($materias)
    {
        $al = Materias::select('id','materia','semestre')
        ->where('semestre',$materias)
        ->get();
          return $al;
    }

    public function formuSemestre(){
        $enviar = Semestre::pluck('semestre','id'); //se asigna a la variable enviar
        return view('AJAX/ejemplo2')->with('semestre',$enviar); //se envia a la vista
    }

//-----------------------------------------------------------------------------------------------------
    //segundas segunda unidad
    public function formuProductos(){
        $enviar = Producto1::pluck('producto','id'); //se asigna a la variable enviar
        return view('AJAX/ejemplo3')->with('producto',$enviar); //se envia a la vista
    }

    public function inserta1(Request $request)
    {
        // 'id','nombre_alumno','numero_control','semestre','materia'
        //$id = 0;
        $producto = $request->input('producto');
        $precio = $request->input('precio');
        $cantidad = $request->input('cantidad');
        $descuento = $request->input('descuento');
        $precio_final = $request->input('precio_final');
        $bandera = 1;

        Producto1::create(['producto' => $producto,'precio' => $precio, 'cantidad' => $cantidad,
        'descuento' => $descuento, 'precio_final' => $precio_final, 'bandera' => $bandera]);
        return redirect()->to('ajaxProductos1');
    }

    public function eliminar_producto($id){
        $hola = Producto1::
        where('id',$id)->take(1)->first();
        return view('AJAX/eliminarProd')->with('dos',$hola);
    }

    public function eliminate($id){
        $editar = Producto1::find($id);
        $editar->delete();
        return redirect()->to('ajaxProductos1');
    }

    //edit_datosP
    public function edit_datosP($id){
        $hola = Producto1::
        where('id',$id)->take(1)->first();
        return view('AJAX/actualizarProd')->with('dos',$hola);
    }

    public function actualizarP(Request $data,$id){
        $editar = Producto1::find($id);
        $editar->id = $data->id;
        $editar->producto = $data->producto;
        $editar->precio = $data->precio;
        $editar->cantidad = $data->cantidad;
        $editar->descuento = $data->descuento;
        $editar->precio_final = $data->precio_final;
        $editar->save();
        return redirect()->to('actualizarP/'.$id);
    }

    //eliminar bandera

    public function eliminar_datosP($id){
        $hola = Producto1::
        where('id',$id)->take(1)->first();
        return view('AJAX/eliminarBP')->with('dos',$hola);
    }

    public function eliminar_P(Request $data,$id){
        $data = Producto1::find($id);
        $data->bandera = '0';
        $data->save();
        return redirect()->to('ajaxProductos1'); 
    }

     
    //---------------------------------------------------------------------------------------------

    public function insertar(Request $request)
    {
        //$id = 0;
        $rfc = $request->input('rfc');
        $razonSocial = $request->input('razonSocial');
        $direccionfiscal = $request->input('direccionfiscal');
        $apoderadolegal = $request->input('apoderadolegal');
        $telefono = $request->input('telefono'); 

        Tiendadeabarrotes::create([/*'id' => 0,*/'rfc' => $rfc,'razonSocial' => $razonSocial,
      'direccionfiscal' => $direccionfiscal,'apoderadolegal' => $apoderadolegal,'telefono' => $telefono]); 
      return redirect()->to('vertienda');
    }

    public function inserta(Request $request)
    {
        // 'id','nombre_alumno','numero_control','semestre','materia'
        //$id = 0;
        $nombre_alumno = $request->input('nombre_alumno');
        $numero_control = $request->input('numero_control');
        $idsemestre = $request->input('idsemestre');
        $idmateria = $request->input('idmateria');

        Alumno::create(['nombre_alumno' => $nombre_alumno,'numero_control' => $numero_control,
        'semestre' => $idsemestre,'materia' => $idmateria]);
        return redirect()->to('ajaxSemestre');
    }

    public function ver_tienda(){
        return view('AJAX/tienda');
    }

    public function insertarproducto(Request $request)
    {
        //$id = 0;
        $nombre = $request->input('nombre');
        $tipo = $request->input('tipo');
        $proveedor = $request->input('proveedor');
        $preciou = $request->input('preciou');
        $preciov = $request->input('preciov'); 

        Producto::create(['nombre' => $nombre,'tipo' => $tipo,
      'proveedor' => $proveedor,'preciou' => $preciou,'preciov' => $preciov]); 
      return redirect()->to('verproducto');
    }

    public function ver_producto(){
        return view('AJAX/producto');
    }


    public function listado_productos($genero)
    {
        $al = Producto::select('id','nombre','tipo','proveedor','preciou','preciov')
        ->where('id',$genero)
        ->get();
          return $al;
    }

    public function formuproducto()
    {
        $enviar = Producto::pluck('nombre','id');
        return view('AJAX/visualizarproducto')->with('sex',$enviar);
    }

    public function formularioa()
    {
        $enviar = Producto::pluck('nombre','id');//se envia el nombre y id 
        return view('AJAX/formulario')->with('sex',$enviar);
    }


    //examen sorpresa
    //editarempresa    
    public function edit_datostienda($id){
        $var1 = Tiendadeabarrotes::where('id',$id)->take(1)->first();
        return view('AJAX/editarempresa')->with('uno',$var1); 
    }

    public function actualizartienda(Request $data,$id){
        $editar = Tiendadeabarrotes::find($id);
        $editar->id = $data->id;
        $editar->rfc = $data->rfc;
        $editar->razonSocial = $data->razonSocial;
        $editar->direccionfiscal = $data->direccionfiscal;
        $editar->apoderadolegal = $data->apoderadolegal;
        $editar->telefono = $data->telefono;
        $editar->save();
        return redirect()->to('actualizartienda/'.$id);
    }

    //eliminar empresa
    public function editar_bandera($id){
        $var1 = Tiendadeabarrotes::
        where('id',$id)->take(1)->first();
        return view('AJAX/eliminartienda')
            ->with('var1',$var1); 
    }

    public function bandera_eliminar($id){
        $editar = Tiendadeabarrotes::find($id);
        $editar->bandera = '0';
        $editar->save();
        return redirect()->to('vertienda');
    }

    //vista ventas
    
}
?>